# Current status of CANVAS
## Higher Ed
### Section file
- Can create section csv file but not on UAT
### Student Enrolment file
- Need to create *inactive* records in enrolment csv file
- Need more test

## English
### Term
- not implement yet - Nga can manually do this
### Course file
- need to create file base on requirement, current course does not work as expected
### Section file
- waiting for requirement confirmation  
### Student Enrolment file
- waiting for requirement confirmation