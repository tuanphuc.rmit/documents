# SQL Queries To Generate Data for Integration Process

## Higher Ed
### Term
```sql
SELECT
CONCAT(RIGHT(`Year`, 2), 's', Sem) as term_id,
CONCAT("Sem ", Sem, ", ", `Year`) as name,
StartDate,
EndDate
FROM srs_academicterms ORDER BY `Year` DESC, Sem ASC
```
### Section
```sql
INSERT IGNORE INTO section_tmp
(
  id,
  courseId,
  courseCode,
  name,
  year,
  semester,
  termCode,
  classGroupId,
  type,
  status,
  startDate,
  endDate,
  BBRowStatus,
  BBAvailableInd,
  hash,
  isValid
)(
	SELECT
	CONCAT(
			i.CourseCode
			, '_',
			CONCAT(SUBSTR(i.`Year`, -2, 2), 's', i.semester), '_',
			LEFT(ca.CommonAbbrev,2), '-',
			IF(g.ClassGroup IS NULL OR g.ClassGroup='', 'NOGP', CONCAT('G', LPAD(g.ClassGroup, 2, '0')))
		) AS id,
	CONCAT(
	  IF(i.BBTranslation is not null AND trim(i.BBTranslation) <> '' AND i.BBTranslation <> i.CourseCode,i.BBTranslation, i.CourseCode),
	  '_',
	  CONCAT(SUBSTR(i.`Year`, -2, 2), 's', i.semester)) AS courseid,
	IF(i.BBTranslation is not null AND trim(i.BBTranslation) <> '' AND i.BBTranslation <> i.CourseCode,i.BBTranslation, i.CourseCode) AS courseCode,
	CONCAT(
			LEFT(ca.CommonAbbrev,2), '-',
			IF(g.ClassGroup IS NULL OR g.ClassGroup='', 'NOGP', CONCAT('G', LPAD(g.ClassGroup, 2, '0'))),
			' (',
			i.CourseCode, '_', CONCAT(SUBSTR(i.`Year`, -2, 2), 's', i.semester), ')'
		) AS `name`,
	i.`Year` AS `year`,
	i.Semester AS semester,
	CONCAT(SUBSTR(i.`Year`, -2, 2), 's', i.semester) AS termcode,

	IF(g.ClassGroup IS NULL OR g.ClassGroup='', 'NOGP', CONCAT('G', LPAD(g.ClassGroup, 2, '0'))) AS classGroupId,
		CASE
			WHEN c.ConcurrentEnglish = 'true' THEN 'CEP'
			ELSE 'HE'
		END AS `type`,
	'active' AS `status`,
	i.CourseStartDate AS startDate,
	i.CourseEndDate AS endDate,
	'enabled' AS BBRowStatus,
	'Y' AS BBAvailableInd,
	NULL AS `hash`,
	true AS isValid
FROM
	srs_bb_courseinstances i
LEFT JOIN
	srs_courses c ON i.CourseCode = c.CourseCode
INNER JOIN
	srs_aps_classgroups g ON g.CourseCode = i.CourseCode AND g.`Year` = i.`Year` AND g.Semester = i.Semester
INNER JOIN
	srs_campuses ca ON ca.CampusCode = g.Campus
WHERE
	i.DoBB = 'true'
	AND i.CourseEndDate > @last365day
	AND i.BBShellDate IS NOT NULL AND i.BBShellDate <= @today
	AND (i.BBTranslation IS NULL OR TRIM(i.BBTranslation) = '')
);
```
### Student Enrolment
```sql
SELECT
  CONCAT(
			re.CourseCode, '_',
			CONCAT(SUBSTR(re.`Year`, -2, 2), 's', re.semester), '_',
			LEFT(ca.CommonAbbrev,2), '-',
			IF(gr.ClassGroup IS NULL OR gr.ClassGroup='', 'NOGP', CONCAT('G', LPAD(gr.ClassGroup, 2, '0'))),
			'-',
			re.studentID
		) AS id, -- 1
  CONCAT(
			   re.CourseCode, '_', CONCAT(SUBSTR(re.`Year`, -2, 2), 's', re.semester), '_',
			LEFT(ca.CommonAbbrev,2), '-',
			IF(gr.ClassGroup IS NULL OR gr.ClassGroup='', 'NOGP', CONCAT('G', LPAD(gr.ClassGroup, 2, '0')))
		) AS sectionId, -- 1
	re.StudentID, -- 2
	CONCAT(re.CourseCode, "_", CONCAT(SUBSTR(re.`Year`, -2, 2), 's', re.semester)) as CourseID, -- 3
	re.CourseCode, -- 4
	CONCAT(LEFT(ca.CommonAbbrev, 2), '-', IF(gr.ClassGroup IS NULL OR gr.ClassGroup='', 'NOGP', CONCAT('G', LPAD(gr.ClassGroup, 2, '0')))) AS classGroupId, -- 5
	re.`year`, -- 6
	re.semester, -- 7
	'HE' AS `type`, -- 8
	'active' AS `status`, -- 9
  -- i.CourseStartDate AS startDate,
	-- i.CourseEndDate AS endDate,
	'enabled' AS BBRowStatus, -- 12
	'Y' AS BBAvailableInd, -- 13
	NULL AS `hash`, -- 14
	true AS isValid -- 15
  -- IF(co.Supplier='RMIT', ci.ClassCode, NULL) AS ClassNr,
	-- IF(co.Supplier='RMIT', co.CourseID, NULL) AS CourseID,
	/*CONCAT_WS(' / ', IF(re.ResultGrade IN ('WDR','RSC'), CONCAT('Post-census withdrawal - ', re.ResultGrade), NULL),
		IF(re.ProgramCode IN ('SAUGD', 'SAPGD'), 'Inb Study Abroad', NULL),
		IF(re.ProgramCode LIKE 'EX%' AND eh.StreamCode='M', 'Inb cross-campus study', NULL),
		IF(re.ProgramCode LIKE 'EX%' AND (eh.StreamCode IS NULL OR eh.StreamCode<>'M'), 'Inb exchange', NULL),
		IF(re.Campus NOT LIKE 'VNMR%' AND re.CourseCode LIKE 'EXTL%' AND co.CourseDesc NOT LIKE '%Study Abroad%', 'Outb exchange to partner', NULL),
		IF(re.Campus NOT LIKE 'VNMR%' AND re.CourseCode NOT LIKE 'EXTL%', 'Outb cross-campus study to RMIT MB', NULL),
		IF(co.CourseDesc LIKE '%Study Abroad%', 'Outb Study Abroad', NULL)
	) AS Comments*/
FROM
srs_results re
INNER JOIN
srs_courses co ON co.CourseCode=re.CourseCode
INNER JOIN
srs_campuses ca ON ca.CampusCode=re.Campus
LEFT JOIN
srs_courseinstances ci ON ci.CourseCode=re.CourseCode AND ci.`Year`=re.`Year` AND ci.Semester=re.Semester AND ci.Campus=re.Campus
LEFT JOIN
srs_enrolmenthistory eh ON eh.StudentID=re.StudentID AND eh.Program=re.ProgramCode AND eh.`Year`=re.`Year` AND eh.Semester=re.Semester AND eh.`Status` IN ('Enrolled', 'Conditional')
LEFT JOIN
srs_aps_classgroups gr ON gr.CourseCode=re.CourseCode AND gr.`Year`=re.`Year` AND gr.Semester=re.Semester AND gr.Campus=re.Campus AND gr.ClassGroup=re.ClassGroup
WHERE
re.`Year`=@CURRENT_YEAR AND re.Semester=@CURRENT_SEM AND re.Variation<>'WDR'
-- exclude placeholder course
AND re.CourseCode NOT IN ('EXCHANGE')
-- exclude credit entries
AND re.ResultGrade NOT IN ('EX','BX','AL','MX','RLG')
-- exclude support courses
AND co.CreditPoints>0 AND (co.FeeCreditsOnly='false' OR co.ConcurrentEnglish='true') #excludesupportline
-- exclude withdrawn enrolments
-- exclude outbound exchange students
AND NOT (re.Campus NOT LIKE 'VNMR%' AND re.CourseCode LIKE 'EXTL%' AND co.CourseDesc NOT LIKE '%Study Abroad%') #excludeoutbexchange
-- this is to exclude outbound cross-campus study
AND NOT (re.Campus NOT LIKE 'VNMR%' AND re.CourseCode NOT LIKE 'EXTL%') #excludeoutbcrosscampus
-- exclude outbound SA
AND co.CourseDesc NOT LIKE '%Study Abroad%' #excludeoutbSA
```
## English
### Term

```sql
SELECT
CONCAT(RIGHT(`Year`, 2), 't', Term) as term_id,
CONCAT("Term ", Term, ", ", `Year`) as name,
StartDate,
EndDate
FROM srs_englishterms WHERE `Year` = YEAR(NOW()) - 1 ORDER BY Term ASC
```

### Course

```sql
SET @today = CURRENT_DATE();
SET @last365day = CURRENT_DATE() - INTERVAL 365 DAY;
SELECT 
CONCAT(
	e.Program, 
	IF(e.Program IN ('TEE', 'IEL'), 
		IF(CHAR_LENGTH(e.ClassLevel) > 2, 
			e.ClassLevel, 
			CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
		),
		e.ClassLevel),
	'_', 
	CONCAT(SUBSTR(e.`Year`, -2, 2), 't', e.Term)
) as course_id,
CONCAT(e.Program, 
	IF(e.Program IN ('TEE', 'IEL'), 
		IF(CHAR_LENGTH(e.ClassLevel) > 2, 
			e.ClassLevel, 
			CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
		),
		e.ClassLevel
	)
) AS name,
e.Term,
e.ClassLevel, 
DATE_SUB(e.ClassCode, INTERVAL 7 DAY), 
DATE_ADD(e.StartDate, INTERVAL 7 DAY), 
e.EndDate, 
e.ClassStream, 
e.Program,
l.Active, e.BBLevelInstancesID 

FROM 
 srs_englishlevelinstances i
INNER JOIN srs_englishterms t ON i.TermID = t.TermID
INNER JOIN srs_englishclasses e ON e.Term = t.Term AND e.Year = t.Year
INNER JOIN srs_englishlevels l ON e.Program = l.Program AND e.ClassLevel = l.LevelCode 

WHERE 
i.DoBB = 'true'
	AND t.EndDate > @last365day
	AND i.BBShellDate IS NOT NULL AND TRIM(i.BBShellDate) != ''
	AND i.BBShellDate <= @today
GROUP BY course_id
```

### Section

```sql
SET @today = CURRENT_DATE();
SET @last365day = CURRENT_DATE() - INTERVAL 365 DAY;

SELECT
	CONCAT(
		l.Program,
		IF(e.Program IN ('TEE', 'IEL'), 
			IF(CHAR_LENGTH(e.ClassLevel) > 2, 
				e.ClassLevel, 
				CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
			),
			e.ClassLevel),
		'_',
		CONCAT(SUBSTR(e.`Year`, -2, 2), 't', e.Term),
		'_',
		-- CASE e.Campus WHEN 'VNMRI' THEN 'S' WHEN 'VNMRH' THEN 'H' WHEN 'VNMRD' THEN 'D' ELSE 'ER' END,
		SUBSTRING_INDEX(e.ClassCode, "-", -1)
	) as section_id,
	
	CONCAT(
			l.Program,
			IF(e.Program IN ('TEE', 'IEL'), 
				IF(CHAR_LENGTH(e.ClassLevel) > 2, 
					e.ClassLevel, 
					CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
				),
				e.ClassLevel),
			'_',
			CONCAT(SUBSTR(t.`Year`, -2, 2), 't', t.Term)
		) AS course_id,
		
	l.levelCode AS courseCode,
	CONCAT(SUBSTRING_INDEX(e.ClassCode,"-", -1),' (',
		l.Program,
		IF(e.Program IN ('TEE', 'IEL'), 
			IF(CHAR_LENGTH(e.ClassLevel) > 2, 
				e.ClassLevel, 
				CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
			),
			e.ClassLevel),
		'_',
		CONCAT(SUBSTR(t.`Year`, -2, 2), 't', t.Term),
		')'
		)
	AS `name`, 
	t.`Year` AS `year`,  
	t.Term AS semester,     
	CONCAT(SUBSTR(t.`Year`, -2, 2), 't', t.Term) AS termcode,
	e.ClassCode AS classCode,  
	'AEP' AS `type`,    
	'active' AS srsStatus,   
	DATE(e.StartDate) AS startDate,  
	DATE(e.EndDate) AS endDate,    
	'enabled' AS BBRowStatus,  
	'Y' AS BBAvailableInd,   
	NULL AS `hash`,   
	TRUE AS isValid    
FROM
	srs_englishlevelinstances i
LEFT JOIN
	srs_englishterms t ON i.TermID = t.TermID
LEFT JOIN
	srs_englishlevels l ON i.LevelID = l.LevelID
INNER JOIN
	srs_englishclasses e ON e.Program = l.Program AND e.ClassLevel = l.LevelCode AND e.`Year` = t.`Year` AND e.Term = t.Term
WHERE
	i.DoBB = 'true'
	AND t.EndDate > @last365day
	AND i.BBShellDate IS NOT NULL AND TRIM(i.BBShellDate) != ''
	AND i.BBShellDate <= @today
;
```

### Student Enrolment

```sql
SELECT 
	CONCAT(
		e.Program,
		IF(e.Program IN ('TEE', 'IEL'), 
			IF(CHAR_LENGTH(e.ClassLevel) > 2, 
				e.ClassLevel, 
				CONCAT(LEFT(e.ClassLevel,1), '0', RIGHT(e.ClassLevel,1))
			),
			e.ClassLevel),
		e.ClassLevel,
		'_',
		CONCAT(SUBSTR(e.`Year`, -2, 2), 't', e.Term),
		'_',
		-- CASE e.Campus WHEN 'VNMRI' THEN 'S' WHEN 'VNMRH' THEN 'H' WHEN 'VNMRD' THEN 'D' ELSE 'ER' END,
		SUBSTRING_INDEX(e.ClassCode, "-", -1)
	) as section_id,
		CONCAT(
			e.Program,
			e.ClassLevel,
			'_',
			CONCAT(SUBSTR(e.`Year`, -2, 2), 't', e.Term)
		) AS course_ID,
-- 	er.ResultID,
	er.StudentID,
	e.ClassCode,
	e.ClassLevel,
-- 	e.Program + LevelCode + "_" + 2_digits_of(Year) + "t" + Term  
	e.Program AS ClassProgram,
	e.ClassGroup,
	e.`Year`,
	e.Term,
	e.StartDate,
	e.EndDate,
	e.Campus
	
FROM srs_englishresults er 
LEFT JOIN srs_englishclasses e USING (ClassCode)
LEFT JOIN srs_applications a ON a.ApplicationID=er.StudentID
LEFT JOIN srs_release_to re ON re.ApplicationID=er.StudentID
WHERE e.`Year`= YEAR(NOW())
GROUP BY er.ResultID
```
